#!/usr/bin/env python

import urllib.parse

def encode_mailto(**kwargs):
    options = {}
    add_options = False

    if (not 'addr' in kwargs) or (kwargs['addr'] == ""):
        return ""

    if 'proto' in kwargs:
        proto = kwargs['proto']
    else:
        proto = "mailto:"

    if 'subject' in kwargs:
        if kwargs['subject'] != "":
            options['subject'] = kwargs['subject']
            add_options = True

    if 'body' in kwargs:
        if kwargs['body'] != "":
            options['body'] = kwargs['body']
            add_options = True

    if add_options:
        res="{}{}?{}".format(proto, kwargs['addr'], urllib.parse.urlencode(options, quote_via=urllib.parse.quote))
    else:
        res="{}{}".format(proto, kwargs['addr'])

    return res

def main():
    import sys, argparse

    parser = argparse.ArgumentParser(description='Urlencode a mailto link from the given parameters or interactively if run with the -i flag.')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-i', '--interactive', action='store_true', help='run in interactive mode')
    group.add_argument('-a', '--address', help='recipient address')
    parser.add_argument('-p', '--protocol', default='mailto:', required=False, help='specify link protocol (defaults to "mailto:")')
    parser.add_argument('-s', '--subject', required=False, help='message subject')
    parser.add_argument('-b', '--body', required=False, help='message body')

    args = parser.parse_args()

    if len(sys.argv) < 2:
        parser.print_usage()

    if args.interactive:
        print("-- Interactive mode --")

        proto = input('Protocol (press enter for "mailto"): ')
        if proto == "":
            proto = 'mailto:'

        addr = input('Address: ')
        subject = input('Subject: ')
        body = input('Body: ')

        print(encode_mailto(proto=proto, addr=addr, subject=subject, body=body))

    elif args.address:
        if args.subject == None:
            args.subject = ""

        if args.body == None:
            args.body = ""

        print(encode_mailto(proto=args.protocol, addr=args.address, subject=args.subject, body=args.body))

if __name__ == "__main__":
    main()
